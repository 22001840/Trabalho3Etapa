//
//  CelulaPersonagem.swift
//  Trabalho 3 etapa
//
//  Created by COTEMIG on 27/10/22.
//

import UIKit

class CelulaPersonagem: UITableViewCell {

    @IBOutlet weak var LabelPersonagem: UILabel!
    @IBOutlet weak var LabelAtor: UILabel!
    @IBOutlet weak var Img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
