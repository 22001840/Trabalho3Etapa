//
//  ViewController.swift
//  Trabalho 3 etapa
//
//  Created by COTEMIG on 20/10/22.
//

import UIKit
import Alamofire
import Kingfisher

class Perosnagem: Decodable {
    let name: String
    let actor: String
    let image: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    
    var userDefaults = UserDefaults.standard

    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ListadePersonagens.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celula = tableView.dequeueReusableCell(withIdentifier: "celulaPotter", for: indexPath) as! CelulaPersonagem
        let personagem = ListadePersonagens [indexPath.row]
        
        celula.LabelPersonagem.text = personagem.name
        celula.LabelAtor.text = personagem.actor
        celula.Img.kf.setImage(with: URL(string: personagem.image))
        
        return celula
    }
    
     
    @IBOutlet weak var TableView: UITableView!
    
    var ListadePersonagens: [Perosnagem] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TableView.dataSource = self
        getNovoPersonagem()
        
    }

    
    func getNovoPersonagem(){
        AF.request("https://hp-api.onrender.com/api/characters").responseDecodable(of: [Perosnagem].self){
            response in
            if let personagem = response.value {
                self.ListadePersonagens = personagem
            }
            print(self.ListadePersonagens)
            self.TableView.reloadData()
        }
        
    }

}


